﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TypicalBackboneWebApp.Models
{
    public class ViewSummary
    {
        public string Page {get; set;}
        public string FromRecords {get; set;}
        public string ToRecords {get; set;}
        public string RecordsPerPage {get; set;}
        public string TotalPages {get; set;}
        public string TotalRecords {get; set;}
        public string Status {get; set;}
        public string SearchRequest { get; set; }
    }
}