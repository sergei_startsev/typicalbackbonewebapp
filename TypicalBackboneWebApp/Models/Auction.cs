﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TypicalBackboneWebApp.Models
{
    public class Auction
    {
        public int AuctionID { get; set; }
        public string RegNumber { get; set; }
        public string AnnouncedDate { get; set; }
        public string AuctionName { get; set; }
        public string Customer { get; set; }
        public string Finance { get; set; }
        public string DeliveryDate { get; set; }
        public string TradeDate { get; set; }
        public string TenderEndDate { get; set; }
        public string AuctionStatusText { get; set; }
        public string AuctionStatusDate { get; set; }
        public string CodeTender { get; set; }
        public string CurrencyAbb { get; set; }
        public int OfferAvailable { get; set; }
        public string OfferAvailableText { get; set; }
        public int OffersAcceptedCount { get; set; }
        public int OffersTotalCount { get; set; }
        public string TotalCost { get; set; }
        public int RequestCount { get; set; }
        public int RequestNoResponceCount { get; set; }

        public Auction()
        {

        }

        public Auction(
            int AuctionID,
            string RegNumber,
            string AnnouncedDate,
            string AuctionName,
            string Customer,
            string Finance,
            string DeliveryDate,
            string TradeDate,
            string TenderEndDate,
            string AuctionStatusText,
            int OfferAvailable,
            string OfferAvailableText,
            int OffersAcceptedCount,
            int OffersTotalCount,
            string TotalCost,
            int RequestCount,
            int RequestNoResponceCount)
        {
            this.AuctionID = AuctionID;
            this.RegNumber = RegNumber;
            this.AnnouncedDate = AnnouncedDate;
            this.AuctionName = AuctionName;
            this.Customer = Customer;
            this.Finance = Finance;
            this.DeliveryDate = DeliveryDate;
            this.TradeDate = TradeDate;
            this.TenderEndDate = TenderEndDate;
            this.AuctionStatusText = AuctionStatusText;
            this.OfferAvailable = OfferAvailable;
            this.OfferAvailableText = OfferAvailableText;
            this.OffersAcceptedCount = OffersAcceptedCount;
            this.OffersTotalCount = OffersTotalCount;
            this.TotalCost = TotalCost;
            this.RequestCount = RequestCount;
            this.RequestNoResponceCount = RequestNoResponceCount;
        }
    }
}