﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TypicalBackboneWebApp.Controllers;

namespace TypicalBackboneWebApp.Handlers
{
    public class ClientErrorHandler : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            Exception ex = ctx.Server.GetLastError();
            ctx.Response.Clear();

            RequestContext rc = ((MvcHandler)ctx.CurrentHandler).RequestContext;
            IController controller = new HomeController(); // Здесь можно использовать любой контроллер
            var context = new ControllerContext(rc, (ControllerBase)controller);

            var viewResult = new ViewResult();

            var httpException = ex as HttpException;
            if (httpException != null)
            {
                switch (httpException.GetHttpCode())
                {
                    //страницы будут искаться в Views/Shared
                    case 404:
                        viewResult.ViewName = "Error404";
                        break;

                    case 500:
                        viewResult.ViewName = "Error500";
                        break;

                    default:
                        viewResult.ViewName = "Error";
                        break;
                }
            }
            else
            {
                viewResult.ViewName = "Error";
            }

            viewResult.ViewBag.Message = filterContext.Exception.Message;

            viewResult.ExecuteResult(context);
            ctx.Server.ClearError();
        }
    }
}