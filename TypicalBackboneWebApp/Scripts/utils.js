﻿var Logger = {
    log: function (msg) {
        if (App.debug && typeof (console) != "undefined") {
            var logStyle = "color:#1E91FC;font-weight:bold;";
            var stackTraceStyle = "color:#555;font-size:.9em;";

            console.log("%c" + msg, logStyle);

            try { throw Error('') } catch (err) {
                if (typeof (err.stack) != "undefined") {
                    var caller_line = err.stack.split("\n")[3];
                    console.log("%c" + caller_line, stackTraceStyle);
                }
            }
        }
    },
    debug: function (msg) {
        if (App.debug && typeof (console) != "undefined") {
            var stackTraceStyle = "color:#555;font-size:.9em;";

            console.debug(msg);

            try { throw Error('') } catch (err) {
                if (typeof (err.stack) != "undefined") {
                    var caller_line = err.stack.split("\n")[3];
                    console.log("%c" + caller_line, stackTraceStyle);
                }
            }
        }
    }
};

var TemplateManager = {
    templates: {},

    get: function (path) {
        var template = this.templates[path];
        if (template) {
            return template;

        } else {
            var that = this;
            $.ajax({
                url: path,
                method: 'GET',
                async: false,
                success: function (template) {
                    that.templates[path] = _.template(template);

                }
            });
            return this.templates[path];
        }
    }

};

var ErrorProcessor = {
    showErrorOnPage: function (xhr, $el) {
        var errorMessage = "<div class='error-message-text'>";
        errorMessage += "<b>Произошла ошибка при запросе данной страницы...</b>";
        errorMessage += "<br/>";
        errorMessage += "<br/>";
        errorMessage += "Ошибка: " + xhr.responseText;
        errorMessage += "<br/>";
        errorMessage += "Код: " + xhr.status + " " + xhr.statusText;
        errorMessage += "<br/>";
        errorMessage += "<br/>";
        errorMessage += "<p style='color:black;'>";
        errorMessage += "Что делать дальше:";
        errorMessage += "<br/>";
        errorMessage += "- обновить страницу "+
        "<span class='no-data-refresh' title='Обновить'><i class='k-icon k-i-refresh'></i></span>," +
        " возможно, проблема была временной";
        errorMessage += "<br/>";
        errorMessage += "- если вы вводили адрес страницы вручную, проверьте его корректность";
        errorMessage += "<br/>";
        errorMessage += "- мы оповещены о вашей проблеме и сделаем все, чтобы это не произошло вновь.";
        errorMessage += "</p>";
        errorMessage += "</div>"
        $el.html(errorMessage);
    }
}