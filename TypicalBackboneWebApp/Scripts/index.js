//Project: **`TypicalBackboneWebApp`**

//**WEB_DEV team** (Центр биржевых информационных технологий)

//- [Sergei Startsev](https://bitbucket.org/sergei_startsev)
//- [Nastassia Lukyanovich](https://bitbucket.org/burbolka)
//- [Igor Philin](https://bitbucket.org/iphilin)
//- [Andrey Lisovskiy](https://bitbucket.org/Andrew_Fox)

// JS libs that are used by this application:
//`jquery`, `backbone`, `underscore`, `kendo ui`.

// ### General structure of the application.
var App = {
    //Set to `false` for production version.
    debug: true,
    //Application root path.
    path: "",

    //Contain description of all [backbone models](http://backbonejs.org/#Model) in this app.
    Models: {
        //Contain instances of our models.
        Instances: {}
    },
    //Contain description of all [backbone collections](http://backbonejs.org/#Collection) in this app.
    Collections: {
        //Contain instances of our collections.
        Instances: {}
    },
    //Contain description of all [backbone routers](http://backbonejs.org/#Router) in this app.
    Routers: {
        //Contain instances of our routers.
        Instances: {}
    },
    //Contain description of all [backbone views](http://backbonejs.org/#View) in this app.
    Views: {
        //Contain instances of our views.
        Instances: {}
    },

    //Application entry point, 
    //`path` - application root path.
    start: function (path) {
        this.path = path;
        //Start [backbone history](http://backbonejs.org/#History-start).
        Backbone.history.start();
    }
};

//### Models

//**ViewSummary model**
App.Models.ViewSummary = Backbone.Model.extend({
    //Default values.
    defaults: function () {
        return {
            fromRecords: "-",
            toRecords: "-",
            totalPages: "-",
            totalRecords: "-"
        };
    }
});

//**State model contains fields which change app state**
App.Models.State = Backbone.Model.extend({
    //Default values.
    defaults: function () {
        return {
            page: "-",
            recordsPerPage: "-",
            status: "100",
            searchRequest: ""
        };
    }
});

//**Registr of auctions**
App.Models.Register = Backbone.Model.extend({
    //Default values, see `App.Models.State` and `App.Models.ViewSummary`.
    defaults: function () {
        return {
            viewSummary: new App.Models.ViewSummary(),
            state: new App.Models.State(),
            //Base data.
            data: ""
        };
    },
    //Initialize model.
    initialize: function () {
        var self = this;
        //Create `change:viewSummary` event if field `viewSummary` was changed.
        this.get("viewSummary").on("change", function () { self.trigger("change:viewSummary") });
        //Create `change:state` event if field `state` was changed.
        this.get("state").on("change", function () { self.trigger("change:state") });
    },
    //Convert this model to JSON format.
    toJSON: function () {
        var attributes = _.clone(this.attributes);
        $.each(attributes, function (key, value) {
            if (_(value.toJSON).isFunction()) {
                attributes[key] = value.toJSON();
            }
        });

        return attributes;
    },
    //Parse data from the server and fill fields.
    parse: function (response) {
        Logger.log("Parse register from the server");

        var setHash = {};

        setHash.data = response.data;

        this.get('viewSummary').set({
            fromRecords: response.viewSummary.FromRecords,
            toRecords: response.viewSummary.ToRecords,
            totalPages: response.viewSummary.TotalPages,
            totalRecords: response.viewSummary.TotalRecords
        });

        this.get('state').set({
            page: response.viewSummary.Page,
            recordsPerPage: response.viewSummary.RecordsPerPage,
            status: response.viewSummary.Status,
            searchRequest: response.viewSummary.SearchRequest
        }, { silent: true });

        return setHash;
    }
});
App.Models.Instances.register = new App.Models.Register();

//Subscribe this model on `change:state` event.
App.Models.Instances.register.on("change:state", function () {
    Logger.log("'change:state' event");

    //Get current model values.
    var state = this.get("state");
    var status = state.get("status");
    var page = state.get("page");
    var searchRequest = state.get("searchRequest");

    var options = {
        url: App.path + "Home/GetAuctionsList",
        type: "POST",
        data: { page: page, status: status, request: searchRequest },
        success: function () {
            Logger.log("Success GetAuctionsList");
        },
        error: function () {
            Logger.log("Error GetAuctionsList");
        }
    }
    //Resets the model state from the server, see [fetch](http://backbonejs.org/#Model-fetch)
    this.fetch(options);
});


//### Routers

//**Main application router**
App.Routers.MainRouter = Backbone.Router.extend({
    routes: {
        "": "emptyNavigation",
        "!/:status": "statusNavigation",
        "!/:status/:page": "pageNavigation",
        "!/:status/:page/:searchRequest": "generalNavigation"
    },

    emptyNavigation: function () {
        Logger.log("Router emptyNavigation");
        this.generalNavigation("100", "1", "");
    },

    statusNavigation: function (status) {
        Logger.log("Router statusNavigation");
        this.generalNavigation(status, "1", "");
    },

    pageNavigation: function (status, page) {
        Logger.log("Router pageNavigation");
        this.generalNavigation(status, page, "");
    },

    generalNavigation: function (status, page, searchRequest) {
        Logger.log("Router generalNavigation");
        var state = App.Models.Instances.register.get("state");
        state.set({ status: status, page: page, searchRequest: searchRequest });
    }
});
App.Routers.Instances.mainRouter = new App.Routers.MainRouter();

//### Views

//**ViewSummary**
App.Views.ViewSummary = Backbone.View.extend({
    //DOM element.
    el: $(".viewSummary"),

    //Initialize model.
    initialize: function () {
        // Subscribe on `change:viewSummary` event.
        this.model.on('change:viewSummary', this.renderViewSummary, this);
    },

    //View templates.
    templates: function (name) {
        var _templates = {
            "viewSummary": "Scripts/templates/viewSummary.html"
        };
        return App.path + _templates[name];
    },

    renderViewSummary: function () {
        Logger.log("Render viewSummary");
        $(this.el).html(TemplateManager.get(this.templates("viewSummary"))(this.model.toJSON()));
        return this;
    }
});
App.Views.Instances.viewSummary = new App.Views.ViewSummary({ model: App.Models.Instances.register });

//**Data view**
App.Views.DataView = Backbone.View.extend({
    //DOM element.
    el: $(".grid-wrapper"),

    //Initialize model.
    initialize: function () {
        // Subscribe on `change:data` event.
        this.model.on('change:data', this.renderData, this);
        // Subscribe on backbone `request` event.
        this.model.on("request", this.request, this);
        // Subscribe on backbone `sync` event.
        this.model.on("sync", this.sync, this);
        // Subscribe on backbone `error` event.
        this.model.on("error", this.error, this);
    },

    //View templates.
    templates: function (name) {
        var _templates = {
            "loading": "Scripts/templates/loading.html",
            "data": "Scripts/templates/data.html"
        };
        return App.path + _templates[name];
    },

    //Subscribe on view events.
    events: {
        "click tr": "selectRow",
        "click .no-data-refresh": "refresh"
    },

    selectRow: function (event) {
        $(".k-state-selected", this.el).removeClass("k-state-selected");
        $(event.target).parent().addClass("k-state-selected");
    },

    refresh: function (event) {
        this.model.trigger("change:state");
    },

    request: function () {
        //Add ajax loader.
        $(".loading-wrapper", $(this.el)).html(TemplateManager.get(this.templates("loading")));
        //Empty `error-message-block` block
        $(".error-message-block", $(this.el)).empty();
    },

    sync: function () {
        //Remove ajax loader.
        $(".loading", $(this.el)).remove();
        //Empty `error-message-block` block
        $(".error-message-block", $(this.el)).empty();
    },

    error: function (model, xhr, options) {
        //Remove ajax loader.
        $(".loading", $(this.el)).remove();
        $(".k-grid", $(this.el)).hide();
        //Show error message on this page
        ErrorProcessor.showErrorOnPage(xhr, $(".error-message-block", $(this.el)));
    },

    renderData: function () {
        Logger.log("Render data");
        //Render data model.
        $(".dataBlock", $(this.el)).html(TemplateManager.get(this.templates("data"))({ data: this.model.get("data") }));
        //If data is empty then hide table and show `no-data` block
        if (this.model.get("data").length == 0) {
            $(".k-grid", $(this.el)).hide();
            $(".no-data", $(this.el)).show();
        } else {
            $(".k-grid", $(this.el)).show();
            $(".no-data", $(this.el)).hide();
        }
        return this;
    }
});
App.Views.Instances.data = new App.Views.DataView({ model: App.Models.Instances.register });

//**Paginator view**
App.Views.PaginatorView = Backbone.View.extend({
    //DOM element.
    el: $(".paginator"),

    //Initialize model.
    initialize: function () {
        // Subscribe on `change:viewSummary` event.
        this.model.on('change:viewSummary', this.renderPaginator, this);
    },

    //View templates.
    templates: function (name) {
        var _templates = {
            "paginator": "Scripts/templates/paginator.html"
        };
        return App.path + _templates[name];
    },

    //Subscribe on view events.
    events: {
        "click a": "navigate"
    },

    navigate: function (event) {
        var state = this.model.get("state");
        var status = state.get("status");
        var searchRequest = state.get("searchRequest");
        //Get page number from link.
        var page = $(event.target).attr("data-page");

        if (searchRequest != "") {
            App.Routers.Instances.mainRouter.navigate("!/" + status + "/" + page + "/" + searchRequest, true);
        } else {
            App.Routers.Instances.mainRouter.navigate("!/" + status + "/" + page, true);
        }
        return false;
    },

    renderPaginator: function () {
        var totalPages = parseInt(this.model.get("viewSummary").get("totalPages"));
        var page = parseInt(this.model.get("state").get("page"));
        //default count of pages that show `<- 1 ... 3 4 5 6 7 ... ->`
        var pageShow = 5;

        var range = Math.floor(pageShow / 2);

        var navBegin = page - range;
        // if even number
        if (pageShow % 2 == 0) {
            navBegin++;
        }
        var navEnd = page + range;

        //right and left ...
        var leftDots = true;
        var rightDots = true;

        //left edge
        if (navBegin <= 2) {
            navBegin = 1;
            leftDots = false;

            if (totalPages < pageShow) {
                navEnd = totalPages;
                rightDots = false;
            } else {
                navEnd = pageShow;
            }
        }

        //right edge
        if (navEnd >= totalPages - 1) {
            navEnd = totalPages;
            rightDots = false;

            if (totalPages - pageShow > 1) {
                navBegin = totalPages - pageShow + 1;
            } else {
                navBegin = 1;
                leftDots = false;
            }
        }

        $(this.el).html(TemplateManager.get(this.templates("paginator"))({
            totalPages: totalPages,
            page: page,
            navBegin: navBegin,
            navEnd: navEnd,
            leftDots: leftDots,
            rightDots: rightDots
        }));
        return this;
    }
});
App.Views.Instances.paginator = new App.Views.PaginatorView({ model: App.Models.Instances.register });

