﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TypicalBackboneWebApp.IGvPurchSrv;
using TypicalBackboneWebApp.Models;
using TypicalBackboneWebApp.Utils;
using TypicalBackboneWebApp.Handlers;

namespace TypicalBackboneWebApp.Controllers
{
    public class HomeController : Controller
    {
        private IGvPurchSrvservice client = new IGvPurchSrvservice();

        struct State
        {
            public ViewSummary viewSummary;
            public List<Auction> data;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ClientAjaxErrorHandler]
        public ActionResult GetAuctionsList(int page, int status, string request)
        {
            List<Auction> auctions = new List<Auction>();
            TViewSummary view = new TViewSummary();
            view.CurrentPage = page;
            if (!request.Equals(""))
            {
                view.SearchStr = request;
            }

            TAuctionsListRec[] list = new TAuctionsListRec[0];

            TAuthHeader header = new TAuthHeader();
            header.SessionID = (String)Session["sessionID"];
            header.SessionKey = (String)Session["sessionKey"];
            client.TAuthHeaderValue = header;

            client.GetAuctionsList(status, false, ref view, ref list);

            for (int i = 0; i < list.Length; i++)
            {
                auctions.Add(new Auction(
                    list[i].AuctionID,
                    list[i].AuctionRegNumber,
                    DateTimeUtils.DoubleToDateTimeToString(list[i].PublishDate, "dd.MM.yyyy"),
                    list[i].SubjectName,
                    list[i].Customer,
                    list[i].FinanceType,
                    DateTimeUtils.DoubleToDateTimeToString(list[i].EndOfferDate, "dd.MM.yyyy"),
                    DateTimeUtils.DoubleToDateTimeToString(list[i].TenderDate, "dd.MM.yyyy HH:mm:ss"),
                    DateTimeUtils.DoubleToDateTimeToString(list[i].TenderEndDate, "dd.MM.yyyy HH:mm:ss"),
                    list[i].AuctionStatusText,
                    list[i].OfferAvailable,
                    list[i].OfferAvailableText.Replace("\n", "<br>"),
                    list[i].OffersAcceptedCount,
                    list[i].OffersTotalCount,
                    list[i].TotalCost.ToString("#,##.####"),
                    list[i].RequestCount,
                    list[i].RequestNoResponceCount
                ));
            }

            ViewSummary viewSummary = new ViewSummary(){
                FromRecords=view.FromRec.ToString(),
                Page=view.CurrentPage.ToString(),
                RecordsPerPage = view.RecPerPage.ToString(),
                SearchRequest = view.SearchStr.ToString(),
                Status = status.ToString(),
                ToRecords = view.ToRec.ToString(),
                TotalPages = view.TotalPages.ToString(),
                TotalRecords = view.TotalCnt.ToString(),
            };

            State state;
            state.data= auctions;
            state.viewSummary = viewSummary;
            return Json(state);
        }
    }
}
